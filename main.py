# https://www.geeksforgeeks.org/python-program-for-dynamic-programming-set-10-0-1-knapsack-problem/
# https://www.youtube.com/watch?v=xOlhR_2QCXY

import numpy as np

def knapSack(W, weights1, values1, weights2, values2, n):
    C = [[0 for x in range(W + 1)] for x in range(n + 1)]

    for row in range(n + 1):
        for column in range(W + 1):
            if row == 0 or column == 0:
                C[row][column] = 0
            elif weights1[row - 1] <= column or weights2[row - 1] <= column:
                first_row_item = 0
                second_row_item = 0

                if weights1[row - 1] <= column:
                    do_not_put_to_knapack_value = C[row - 1][column]
                    put_to_knapack_value = values1[row - 1] + C[row - 1][column - weights1[row - 1]]

                    first_row_item = max(put_to_knapack_value, do_not_put_to_knapack_value)
                if weights2[row - 1] <= column:
                    do_not_put_to_knapack_value = C[row - 1][column]
                    put_to_knapack_value = values2[row - 1] + C[row - 1][column - weights2[row - 1]]

                    second_row_item = max(put_to_knapack_value, do_not_put_to_knapack_value)

                C[row][column] = max(first_row_item, second_row_item)
            else:
                C[row][column] = C[row - 1][column]

    np.savetxt('output.txt', C, fmt='%5.0f')
    return C[n][W]


file_path = "input.txt"

with open(file_path) as f:
    lines = f.readlines()

lines = [x.strip() for x in lines]

values1 = []
weights1 = []
values2 = []
weights2 = []
max_weight = 2000

for line in lines:
    values1.append(int(line.split(",")[0]))
    weights1.append(int(line.split(",")[1]))
    values2.append(int(line.split(",")[2]))
    weights2.append(int(line.split(",")[3]))

print("weights1 " + str(weights1))
print("values1 " + str(values1))
print("weights2 " + str(weights2))
print("values2 " + str(values2))

print("The biggest value is " + str(knapSack(max_weight, weights1, values1, weights2, values2, len(values1))))
